import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MyArrayListGetTests {

    @Test
    public void get_should_returnLastPosition_when_getLastUsedPosition() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        list.add(1);
        list.add(13);
        list.add(5);
        list.add(42);
        // Act
        int result = list.getLast();
        // Assert
        Assertions.assertEquals(42, result);

    }

    @Test
    public void get_should_returnFirstPosition_when_getFirstPosition() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        list.add(12);
        list.add(13);
        // Act
        int result = list.getFirst();
        // Assert
        Assertions.assertEquals(12, result);

    }

    @Test
    public void get_should_returnUsedPosition_when_getUsedPosition() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        list.add(12);
        list.add(4);
        // Act
        int result = list.getUsedPositions();
        // Assert
        Assertions.assertEquals(2, result);

    }

    @Test
    public void get_should_returnRightSize_when_getSize() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        // Act
        int result = list.getSize();
        // Assert
        Assertions.assertEquals(4, result);

    }
}
