
import com.telerikacademy.MyArrayList;
import com.telerikacademy.MyList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MyArrayListTests {


    @Test
    public void get_should_returnRightElement_when_IndexIsValid() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        list.add(1);
        list.add(2);
        // Act
        int result = list.get(0);

        // Assert
        Assertions.assertEquals(1, result);
    }

    @Test
    public void get_should_throw_when_indexIsBelowZero() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(-1));
    }

    @Test
    public void get_should_throw_when_indexIsAboveTheUpperBound() {
        // Arrange
        MyList<Integer> list = new MyArrayList<>();
        // Act Assert
        Assertions.assertThrows(IndexOutOfBoundsException.class,
                () -> list.get(0));

    }

    @Test
    public void get_should_returnValidIndex_when_setIndex() {
        // Arrange

        // Act


        // Assert

    }

}
