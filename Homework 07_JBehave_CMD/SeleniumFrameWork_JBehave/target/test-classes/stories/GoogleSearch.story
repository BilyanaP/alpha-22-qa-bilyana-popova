Meta:
@googleSearch

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Search in google
Given SwitchTo search.ConsentButton element
When Click search.AgreeButton element
When Wait for search.Input
When Click search.Input element
When Type Telerik Academy in search.Input field
When Wait for search.Button
When Click search.Button element
When Wait for search.Result
Then Click search.Result element
