Meta:
@qa

Narrative:
As a future QA
I want to find info about Alpha QA program at Teleric Academy
So that I want to learn about next admission of the program

Scenario: Find admission page in Teleric Academy website

When Click academy.PrivacyPolicyButton element
When Wait for academy.AlphaAnchor
When Click academy.AlphaAnchor element
When Wait for academy.QaGetReadyLink
When Click academy.QaGetReadyLink element
When Wait for academy.SignUpNavButton
Then Click academy.SignUpNavButton element