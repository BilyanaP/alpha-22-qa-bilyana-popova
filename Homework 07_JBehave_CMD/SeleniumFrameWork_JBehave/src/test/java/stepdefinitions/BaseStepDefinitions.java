package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.BeforeStories;
import org.jbehave.core.annotations.When;

public class BaseStepDefinitions {
        UserActions actions = new UserActions();

    @BeforeStories
    public void beforeStories(){
        UserActions.loadBrowser();
    }

//    @AfterStories
//    public void afterStories(){
//        UserActions.quitDriver();
//    }
}
