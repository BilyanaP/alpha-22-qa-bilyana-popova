package stepdefinitions;

import com.telerikacademy.testframework.UserActions;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;

public class StepDefinitions extends BaseStepDefinitions {
    UserActions actions = new UserActions();

    @Given("Click $element element")
    @When("Click $element element")
    @Then("Click $element element")
    public void clickElement(String element) {
        actions.clickElement(element);
    }

    @Given("Type $value in $name field")
    @When("Type $value in $name field")
    @Then("Type $value in $name field")
    public void typeInField(String value, String name) {
        actions.typeValueInField(value, name);
    }

    @When("Wait for $element")
    public void elementPresent(String element) {
        actions.waitForElementVisible(element, 10);
    }

    @Given("SwitchTo $key element")
    public void switchToIFrame(String key) {
        actions.switchToIFrame(key);
    }
    @When("SwitchTo default element")
    public void switchToDefault() {
        actions.returnToDefaultContent();
    }
}
