package testCases;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.GooglePage;

public class SearchInGooglePage extends BaseTest {
    String searchCriterion = "Telerik Academy";

    @Test
    public void simpleGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());

        google.searchAndAgreeWithConsentButton();
        google.SearchAndOpenFirstResult(searchCriterion);

        google.AssertTelerikAcademyPageNavigated();


    }


}
