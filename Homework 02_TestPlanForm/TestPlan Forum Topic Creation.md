
# Forum.Fishing-mania Create topic Test Plan 

# Prepared by:
Bilyana Popova
26.08.2020

# Test Plan
* The purpose of this document is to create a working and communication plan of the team, which will test working quality and functionality for creating topics in the forum https://forum.fishing-mania.com/

* The tasks cover to clear all requiremets of the client, used testing levels and types, time-frames, exit criteria, hardware and environment requirements, features to be and not to be tested according to Test Process.

## Testing types
###  Unit testing - 
- The implementation of unit tests is a requirement to developers.  
- Follow-up from QA specialist.
- The Unit tests should cover more than 95 % of the newly developed code. Unit test scenario can be reviewed to ensure coverage all relevant cases.
- Unit tests should live in the Source control and should be executed after each check-in in the code. All tests must pass before the code is uploaded on the Integration/SIT environment.

### System and Integration Testing
* Clear requirements on the functionality under test.
* QA will be prepare test cases based on requirements (e.g. 
    - Choosing right category to create topic.
    - Create topic if user is or not registered.
    - Check if there is a similar topic.)
* The test cases should have Title, Steps to reproduce, Priority.
* The integration should be between selected methods and components in the code, between code and external libraries, integration between the different layers - front end, middle tiers, data lyers.

### Usability testing
* Will checked from the Marketing team of the Client.
* The Acceptance criterion could be logged in the following format: 
AS A [ACTOR]
I NEED TO BE ABLE TO [ACTION]
SO THAT I CAN [AIM]

Example: 
AS A user create new topic at https://forum.fishing-mania.com/
I NEED TO BE ABLE TO check if similar topic is exist and return yes or no less than a minute.
SO THAT I CAN to receave information and create new topic with comment.


### Security testing
- No XSS allowed in the website forms
- Exception handling that is not exposing backend code
- IIS Tracking of users trying to flood the website
- Backend security

* Security testing sessions to be performed before each website release. The cases verified to be described in the Test Case Management system. The issues found to be logged with HIGH priority in the Product backlog

### Regression testing
* The goal for the Regression suite is to verify that no regression issues were introduced in the most important pages and functionalities. The execution of the tests should be under 1 hour so that we can receive rapid feedback on the website status after each check-in. All automated tests and test data should be under source control.

### Performance Testing
* The tests should be run with no more than 20 concurrent users. Their goal is to create load time statistics that are as close to the real one as possible. 

## Environment requirements
* Windows: Latest version of Chrome, Firefox, Edge, Safari
* MAC OS: Latest version of Chrome, Firefox, Safari
* Linux: Latest version of Chrome, Firefox

The responsiveness of the design will be checked under the following mobile devices:
* The newest models of Samsung, Huawei, Nokia, Xiaomi, Apple iPhone

##  Schedule
* Planning and control - 3 man days.
* Anlysis and design - 6 man days.
* Test implementation and Execution - 6 man days. 
* Exit Criteria and Reporting - 2 man days.
* Closure - 1 man day.

## Exit Criteria
* When all blocked, critical and high-risk areas have been fully tested.
* When a test plan has been achieved.
* When time runs out.

## Scope of testing
### Functionalities to be tested
* Special requirements provided by Client.
* Easy Search and Filters Logic
* Performance

### Functionalities not to be tested
* Usability testing
* Тests not described in the Test plan

## APPROVALS
* Project manager
* Developer
* QA

Date:
## End.