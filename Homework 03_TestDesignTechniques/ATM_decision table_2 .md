﻿| Conditions                               | Values | 1 | 2 | 3       | 4 | 5       | 6 | 7 | 8 |
|------------------------------------------|--------|---|---|---------|---|---------|---|---|---|
| Insert valid card and money is available | Y/N    | Y | Y | Y       | Y | N       | N | N | N |
| PIN is correct                           | Y/N    | Y | Y | N       | N | ~ \(Y\) | Y | N | N |
| Insert incorrect PIN three times         | Y/N    | Y | N | ~ \(N\) | N | ~ \(N\) | N | Y | N |
| Actions                                  | Values |   |   |         |   |         |   |   |   |
| Ask new PIN                              | Y/N    | N | N | Y       | Y | N       | N | N | N |
| Eat card                                 | Y/N    | Y | N | N       | Y | N       | N | N | N |
| Pay money                                | Y/N    | N | Y | N       | N | N       | N | N | N |
