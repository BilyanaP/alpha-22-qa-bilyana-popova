﻿| Conditions                                    | Values | 1 | 2 | 3       | 4 | 5       | 6 | 7 | 8 |
|-----------------------------------------------|--------|---|---|---------|---|---------|---|---|---|
| Card is inserted and read                     | Y/N    | Y | Y | Y       | Y | N       | N | N | N |
| PIN entered is correct                        | Y/N    | Y | Y | N       | N | ~ \(Y\) | Y | N | N |
| Requested amount is available in card account | Y/N    | Y | N | ~ \(Y\) | N | ~ \(N\) | N | Y | N |
|  Expected Actions                             | Values |   |   |         |   |         |   |   |   |
| Funds are withdrawn                           | Y/N    | Y | N | N       | N | N       | N | N | N |
