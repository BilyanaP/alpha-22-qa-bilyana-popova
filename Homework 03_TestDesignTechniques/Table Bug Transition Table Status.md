﻿| State          | Event                                  | Action                                                                        | New state              |
|----------------|----------------------------------------|-------------------------------------------------------------------------------|------------------------|
| None           | Open new issue                         | New issue is found                                                            | Status  New            |
| New            | The issue is approved                  | The bug is assigned to the developer team                                     | Status  Assigned       |
| Assigned       |  The issue is analized from dev teams  | Dev teams started working on issue                                            | Status  Open           |
| Open           | Dev works on it                        | Dev makes necessary code changes and verifies them                            | Status  Fixed          |
| Fixed          | Dev fixed the issue                    | Dev send the fixed issue to QA tester to retest it                            | Status  Pending retest |
| Pending retest | QA tester retests the code             | QA tester checks whether the issue is fixed or not                            |  Status Retest         |
| Retest         | QA tester tests the issue again        | Issue  is not present in the software\. QA tester approves the bug is fixed\. | Status Verified        |
| Verified       | The issue is fixed, tested and aproved | The issue is no longer exist in the sofware                                   | Status Closed          |
