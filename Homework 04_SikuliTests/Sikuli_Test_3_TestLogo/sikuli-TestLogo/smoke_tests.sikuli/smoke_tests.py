from _lib import *

    
class SmokeTests(unittest.TestCase):
    
    def setUp(self):
        pass
    
    def tearDown(self):
        pass    

    def test_101_Open_Chrome(self):
        TestLogo.StartChromeIncognitoForum()

    def test_105_Test_Telerik_Logo(self):
        assert(exists(LogoUI.telerik_logo_img).highlight(2))

    def test_110_Close_Chrome(self):
        TestLogo.CloseChrome()  




if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SmokeTests)
    
    print("WARNING: You may need to adapt UI map to match your windows specifics")
    outfile = open("Report.html", "w")
    runner = HTMLTestRunner.HTMLTestRunner(stream=outfile, title='SmokeTests Report' )
    runner.run(suite)
    outfile.close()

