from sikuli import *
import unittest
import iHTMLTestRunner.iHTMLTestRunner as HTMLTestRunner
from _uimap import *

class TestLogo:
	@classmethod
	def StartChromeIncognitoForum(self):
		type('r', KeyModifier.WIN)
		sleep(2)
		type('chrome -incognito https://forum.telerikacademy.com')
		sleep(2)
		type(Key.ENTER)
		sleep(10)

	@classmethod
	def CloseChrome(self):
		type(Key.F4, KeyModifier.ALT)
