
# Create test cases covering the following functionality of a forum:

## Comments:

1.	Check user login with valid email or username and password.
2.	Check right path to the topic.
3.	Choosing the right topic in the forum.
4.	Add last comment in the end of the topic.
5.	Check if the response is to the exact comment.
6.	Check if sent comment is blank.
7.	Check a comment with attachment.
8.	Check to send the high volume attachment.
9.	Check a comment with high volume.
10.	Check if a comment is in Cyrillic.
11.	Check preview of a comment.
12.	Check comments contain obscene and offensive words.
13.	Observance of the rules and General Terms of the forum.
14.	Grammar check.

## Creation of topics:

1.	Check user login with valid email or username and password.
2.	Choosing right category to create topic.
3.	Choosing right sub category to create topic.
4.	Create topic if user is registered.
5.	Create topic if user is Not registered.
6.	Checking user rights.
7.	Check the rights of the forum administrator
8.	Check if there is a similar topic.
9.	Unable to create a topic with empty subject.
10.	Check if comment’s field is empty.
11.	Check a new topic with attachment.
12.	Check to send the high volume attachment.
13.	Check a comment’s field with high volume.
14.	Check if a subject and comment is in Cyrillic.
15.	Check lowercase and uppercase letters of the new title.
16.	Check creating survey.
17.	Grammar check.
