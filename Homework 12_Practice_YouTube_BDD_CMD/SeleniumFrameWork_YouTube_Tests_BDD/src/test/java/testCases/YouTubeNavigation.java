package testCases;

import pages.GooglePage;
import pages.YouTubePage;
import org.junit.Test;

import java.util.concurrent.TimeUnit;

public class YouTubeNavigation extends BaseTest {
    String searchCriterion = "You Tube";
    String searchVideo = "Michael Jackson - Billie Jean";

    @Test
    public void navigateToCourseFromGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());
        YouTubePage youTube = new YouTubePage(actions.getDriver());

        google.searchAndAgreeWithConsentButtonGooglePage();
        google.SearchAndOpenFirstResult(searchCriterion);
        actions.implicitWait();

        youTube.searchAndAgreeWithConsentButtonYouTubePage();
        youTube.navigatedToVideoAtYouTube(searchVideo);

        youTube.AssertCurrentVideoPageNavigated();

    }
}
