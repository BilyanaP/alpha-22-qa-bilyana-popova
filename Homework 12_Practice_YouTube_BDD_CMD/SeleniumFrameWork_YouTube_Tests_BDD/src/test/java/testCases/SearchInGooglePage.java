package testCases;

import org.junit.Test;
import pages.GooglePage;

public class SearchInGooglePage extends BaseTest {
    String searchCriterion = "You Tube";

    @Test
    public void simpleGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());

        google.searchAndAgreeWithConsentButtonGooglePage();
        google.SearchAndOpenFirstResult(searchCriterion);

        google.AssertYouTubePageNavigated();


    }



}
