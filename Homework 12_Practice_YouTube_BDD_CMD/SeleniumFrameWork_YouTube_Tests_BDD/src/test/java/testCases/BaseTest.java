package testCases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {
    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser();
    }

	@AfterClass
	public static void tearDown(){
		UserActions.quitDriver();
	}
    }
