package pages;

import com.telerikacademy.testframework.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YouTubePage extends BasePage {
    final String PAGE_URL = Utils.getConfigPropertyByKey("academy.url");


    public YouTubePage(WebDriver driver) {
        super(driver);
        super.setUrl(PAGE_URL);
    }

    public void searchAndAgreeWithConsentButtonYouTubePage() {
        actions.clickElement("search.NoThanksButton.YouTube");
//        actions.switchToIFrame("search.ConsentButton.YouTube");
//        actions.clickElement("search.AgreeButton.YouTube");
//        actions.returnToDefaultContent();
    }


    public void navigatedToVideoAtYouTube(String searchVideo) {
        actions.typeValueInFieldAndClickEnter(searchVideo,"youtube.SearchInField");
        actions.clickElement("youtube.SearchResult");
        actions.waitMillSec(15000);
        actions.clickElement("youtube.PauseButton");
        actions.waitMillSec(1000);
        actions.clickElement("youtube.FullScreenButton");
        actions.waitMillSec(1000);
        actions.clickElement("youtube.ExitFullScreenButton");

    }

    public void AssertCurrentVideoPageNavigated() {
        actions.assertNavigatedUrl("youtube.CurrentVideoNavigated");
    }

}