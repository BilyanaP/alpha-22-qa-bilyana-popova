package testCases;

import Pages.GoogleHomePage;
import Pages.YouTubePage;
import com.telerikacademy.SeleniumWebDriver_Test.UserActions;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;


import java.util.concurrent.TimeUnit;

public class YouTubeNavigation extends BaseTests {

    private static final String PAGE_URL = "https://youtube.com";
    public String logoYouTube = "//div[@id='start']//a[@id='logo']";
    public String currentUrl = "https://www.youtube.com/watch?v=Zi_XLOBDo_Y";

    @Test
    public void navigateToVideoAtYouTube() {
        driver.get(PAGE_URL);
        YouTubePage myYouTubePage = new YouTubePage(driver);

        myYouTubePage.agreeWithConsent();
        actions.assertElementPresent(logoYouTube);

        myYouTubePage.navigatedToVideoAtYouTube();
        actions.assertResultPageNavigated(currentUrl);

        actions.waitMillSec(30000);
    }
}

