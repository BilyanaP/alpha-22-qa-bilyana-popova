package testCases;

import Pages.GoogleHomePage;
import com.telerikacademy.SeleniumWebDriver_Test.UserActions;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;


public class SearchInGooglePage extends BaseTests{
    private String currentUrl = "https://www.youtube.com";

    @Test
    public void googleSearchPageNavigated_When_SearchIsPerformed() {
        GoogleHomePage homePage = new GoogleHomePage(driver);

        homePage.agreeWithConsent();

        actions.typeValueInField("You Tube", homePage.searchTextBox);
        actions.clickOnElement(homePage.searchButton);
        actions.clickOnElement(homePage.searchResult);

        actions.assertResultPageNavigated(currentUrl);

    }

}


