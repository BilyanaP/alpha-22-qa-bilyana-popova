package testCases;

import com.telerikacademy.SeleniumWebDriver_Test.UserActions;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.concurrent.TimeUnit;

public class BaseTests {

    private static final String GOOGLE_SEARCH_URL = "https://google.com";
    public WebDriver driver;
    public UserActions actions;


    @BeforeClass
    public static void classInit() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void testInit() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        driver = new ChromeDriver(options);
        actions = new UserActions(driver);
        driver.get(GOOGLE_SEARCH_URL);

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
    public void testCleaner() {
        driver.close();
    }
}
