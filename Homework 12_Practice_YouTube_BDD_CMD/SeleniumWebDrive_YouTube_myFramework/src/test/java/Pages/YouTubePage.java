package Pages;

import com.telerikacademy.SeleniumWebDriver_Test.UserActions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class YouTubePage {

    private final WebDriver driver;
    private final UserActions actions;
    public String noThanksButton = "//paper-button[@aria-label='No thanks']";
    public String iFrame = "//paper-dialog[@id='dialog']/iframe";
    public String agreeButton = "//div[@id='introAgreeButton']";
    public String logoYouTube = "//div[@id='start']//a[@id='logo']";
    public String searchField = "//input[@id='search']";
    public String searchResult = "//div[@id='title-wrapper']//yt-formatted-string[text()='Michael Jackson - Billie Jean (Official Video)']";
    public String pauseButton = "//button[@title='Pause (k)']";
    public String fullScreen = "//button[@title='Full screen (f)']";
    public String exitFullScreen = "//button[@title='Exit full screen (f)']";


    public YouTubePage(WebDriver driver) {
        this.driver = driver;
        actions = new UserActions(driver);
    }

    public void agreeWithConsent() {
        actions.clickOnElement(noThanksButton);
        actions.switchToFrame(iFrame);
        actions.clickOnElement(agreeButton);
        actions.switchToDefaultContent();
    }

    public void navigatedToVideoAtYouTube() {
        WebElement searchTextBox = driver.findElement(By.xpath(searchField));
        searchTextBox.sendKeys("Michael Jackson - Billie Jean" + Keys.ENTER);
        actions.clickOnElement(searchResult);
        actions.clickOnElement(pauseButton);
        actions.clickOnElement(fullScreen);
        actions.clickOnElement(exitFullScreen);

    }

}
