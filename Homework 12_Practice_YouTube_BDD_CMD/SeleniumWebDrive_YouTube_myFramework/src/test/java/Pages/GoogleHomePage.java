package Pages;

import com.telerikacademy.SeleniumWebDriver_Test.UserActions;
import org.junit.*;
import org.openqa.selenium.WebDriver;


public class GoogleHomePage {

    private final WebDriver driver;
    private final UserActions actions;


    public GoogleHomePage(WebDriver driver) {
        this.driver = driver;
        actions = new UserActions(driver);
    }

    public String searchTextBox = "//input[@name='q']";
    public String searchButton = "//input[@name='btnK']";
    public String iFrame = "//div[@id='cnsw']/iframe";
    public String agreeButton = "//div[@id='introAgreeButton']";
    public String searchResult = "//cite[text()='www.youtube.com']";
    public String currentUrl = "https://www.youtube.com";


    public void agreeWithConsent() {
        actions.switchToFrame(iFrame);
        actions.clickOnElement(agreeButton);
        actions.switchToDefaultContent();
    }
}
