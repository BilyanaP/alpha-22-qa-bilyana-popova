package com.telerikacademy.SeleniumWebDriver_Test;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class UserActions {

    private final WebDriver driver;

    public UserActions(WebDriver driver) {
        this.driver = driver;
    }

    // ########### USER ACTIONS ###########

    public void clickOnElement(String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.click();
    }

    public void typeValueInField(String value, String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        element.sendKeys(value);
    }

    // ########### DRIVER ACTIONS ###########


    public void switchToFrame(String xpath) {
        WebElement element = driver.findElement(By.xpath(xpath));
        driver.switchTo().frame(element);
    }

    public void switchToDefaultContent() {
        driver.switchTo().defaultContent();
    }

    public void assertElementPresent(String xpath) {
        Assert.assertNotNull(driver.findElement(By.xpath(xpath)));
    }

    // ########### WAIT ###########

    public void waitMillSec(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // ########### ASSERT ###########

    public void assertResultPageNavigated(String currentUrl) {
        Assert.assertTrue("Page was not navigated. Actual url: " + driver.getCurrentUrl() +
                        "Expected url:" + currentUrl,
                driver.getCurrentUrl().contains(currentUrl));
    }
}
