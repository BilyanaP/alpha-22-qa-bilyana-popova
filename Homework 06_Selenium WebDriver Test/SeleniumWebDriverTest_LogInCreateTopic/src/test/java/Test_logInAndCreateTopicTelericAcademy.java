import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Test_logInAndCreateTopicTelericAcademy {

    private static final String googleSearchUrl = "https://google.com";
    private static final String stageForumTelerikUrl = "https://stage-forum.telerikacademy.com/";
    private static final String logInButton = "//div[@class='wrap']//span[@class='d-button-label']";
    private static final String idEmail = "Email";
    private static final String idPass = "Password";
    private static final String email = "bpopova1@gmail.com";
    private static final String pass = "TestTelerik";

    private static final String idCreateNewTopicButton = "create-topic";
    private static final String idTopicTitle = "reply-title";
    private static final String title = "Hello Web DriverSelenium";
    private static final String insertTextField = "//textarea[@class='d-editor-input ember-text-area ember-view']";

    private WebDriver driver;

    @BeforeClass
    public static void classInit() {
        System.setProperty("webdriver.chrome.driver", "D:\\QA\\chromedriver_win32\\chromedriver.exe");
    }

    @Before
    public void testInit() {
        driver = new ChromeDriver();
        driver.get(googleSearchUrl);
        agreeWithConsent(driver);
    }

    @Test
    public void googleHomePageNavigated_When_GoogleOpened() {

        String expectedHomePageUrl = "https://www.google.com/";
        Assert.assertEquals("Page was not navigated", driver.getCurrentUrl(), expectedHomePageUrl);
    }

    @Test
    public void openPageStageForumTelerik_When_PageIsFound() {

        stageForumTelerikOpened();
        waitSec(2000);

        Assert.assertTrue("xpath=//img[@class='logo-big']", true);
        waitSec(2000);
    }

    @Test
    public void LogInPageStageForumTelerik_When_PageisOpened() {

        stageForumTelerikOpened();
        waitSec(2000);

        clickLogInButton(logInButton);

        Assert.assertTrue("xpath=//div[@class='entry-item']", true);
    }

    @Test
    public void LogInEmailAndPass_When_LogInPageisOpened() {

        stageForumTelerikOpened();

        waitSec(2000);

        clickLogInButton(logInButton);

        logInEmailAndPass();

        Assert.assertTrue("xpath=//div[@class='panel clearfix']//img[@class='avatar']", true);
    }

    @Test
    public void CreateNewTopic_When_LogInPageIsOpened() {

        stageForumTelerikOpened();

        waitSec(2000);

        clickLogInButton(logInButton);

        logInEmailAndPass();

        createNewTopic();

        WebElement topicButton = driver.findElement(By.xpath("//div[@class='save-or-cancel']//span[@class='d-button-label']"));
        topicButton.click();

        waitSec(2000);

        String actualTopicTitle = driver.findElement(By.xpath("//div[@class='title-wrapper']//a[@class='fancy-title']")).getText();
        Assert.assertTrue(actualTopicTitle.contains("Hello"));

    }

        @After
    public void testCleaner() {
        driver.close();
    }

    private void waitSec(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void agreeWithConsent(WebDriver driver) {
        WebElement iFrame = driver.findElement(By.xpath("//div[@id ='cnsw']")).findElement(By.tagName("iFrame"));
        driver.switchTo().frame(iFrame);
        WebElement agreeButton = driver.findElement(By.id("introAgreeButton"));
        agreeButton.click();
        driver.switchTo().defaultContent();
    }

    private void stageForumTelerikOpened() {
        driver.get(stageForumTelerikUrl);
    }

    private void clickLogInButton(String logInButton) {
        WebElement logIn = driver.findElement(By.xpath(logInButton));
        logIn.click();
    }

    private void logInEmailAndPass() {
        WebElement enterEmail = driver.findElement(By.id(idEmail));
        enterEmail.sendKeys(email);
        WebElement enterPass = driver.findElement(By.id(idPass));
        enterPass.click();
        enterPass.sendKeys(pass + Keys.ENTER);
    }

    private void createNewTopic() {
        WebElement createNewTopicButton = driver.findElement(By.id(idCreateNewTopicButton));
        createNewTopicButton.click();
        WebElement createTopicTitle = driver.findElement(By.id(idTopicTitle));
        createTopicTitle.sendKeys(title);
        WebElement insertText = driver.findElement(By.xpath(insertTextField));
        insertText.click();
        insertText.sendKeys(title);
    }
}
