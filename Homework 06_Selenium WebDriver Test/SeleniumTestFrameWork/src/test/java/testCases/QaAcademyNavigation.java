package testCases;

import pages.GooglePage;
import pages.TelerikAcademyPage;
import org.junit.Test;

public class QaAcademyNavigation extends BaseTest {
    String searchCriterion = "Telerik Academy";

    @Test
    public void navigateToCourseFromGoogleSearch() {
        GooglePage google = new GooglePage(actions.getDriver());

        TelerikAcademyPage academy = new TelerikAcademyPage(actions.getDriver());

        actions.switchToIFrame("search.ConsentButton");
        actions.clickElement("search.AgreeButton");
        actions.returnToDefaultContent();

        google.SearchAndOpenFirstResult(searchCriterion);


        academy.NavigateToQACourseViaCard();

        academy.AssertQAAcademySignupPageNavigated();


    }
}
